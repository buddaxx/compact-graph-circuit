#ifndef _PORTE_BASE_
#define _PORTE_BASE_

#include <iostream>

using namespace std;

// class de base

class PORTE_BASE {

/* interface  */
protected:
	PORTE_BASE (){};

private:
    int nombre_input;
	char *p_input;
	int output;

public:

    friend class GRAPHE;

	virtual ~PORTE_BASE(){};
	virtual void print_info()=0;
    virtual void Create_input(int n)=0;
	virtual void Set_input(char val, int i)=0;
	virtual int Calculate_output()=0;
};

// classes derivee

class PORTE_AND : public PORTE_BASE {

private:
    int nombre_input;
	char *p_input;
	int output;

public:

    friend class GRAPHE;

	PORTE_AND();
    ~PORTE_AND();

    virtual void Create_input(int n);
    virtual void print_info();
	virtual void Set_input(char val,int i);
	virtual int Calculate_output();
};



class PORTE_OR : public PORTE_BASE {

private:
    int nombre_input;
	char *p_input;
	int output;

public:

    friend class GRAPHE;

	PORTE_OR ();
    ~PORTE_OR();
    virtual void Create_input(int n);
    virtual void print_info();
	virtual void Set_input (char val,int i);
	virtual int Calculate_output();
};

class PORTE_NOT : public PORTE_BASE
{

private:
    int nombre_input;
	char *p_input;
	int output;

public:

    friend class GRAPHE;

	PORTE_NOT ();
    ~PORTE_NOT ();
    virtual void Create_input(int n);
    virtual void print_info();
	virtual void Set_input(char val,int i);
	virtual int Calculate_output();
};



class PORTE_XOR : public PORTE_BASE
{

private:
    int nombre_input;
	char *p_input;
	int output;

public:

    friend class GRAPHE;

	PORTE_XOR();
    ~PORTE_XOR();
    virtual void Create_input(int n);
    virtual void print_info();
	virtual void Set_input(char val, int i);
    virtual int Calculate_output();
};

#endif
