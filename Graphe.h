#ifndef _GRAPHE_
#define _GRAPHE_

#include <iostream>
#include <vector>
#include "PORTE_Base.h"
#include "Valeur_Logique.h"

using namespace std;

class GRAPHE{

    private:
        int n_entree;
        int n_sortie;
        int n_porte;
        int N; // indice de matrice
        typedef vector<int> lin;
        vector<lin> M;
        int nlin,nmot;

    public:
        vector<string> ligne; // sauvegarder le fichier ligne par ligne
        vector<string> mot; // sauvegarder le fichier mot par mot


        GRAPHE(); //Constructeur par defaut
        ~GRAPHE(); // Destructeur au cas ou on a un delete a faire
        void Set_Info(int n_ent, int n_sor, int n_por);
        void Set_nlin_nmot(int nl, int nm);
        void Set_Matrice(int mlin, int mcol);
        void Init_Matrice();
        void Print_Info();
        void Print_Matrice();
        void DFS_Methode(VALEUR &MyValeur, PORTE_BASE *P_MyPortes[]);
};

#endif // _GRAPHE_
