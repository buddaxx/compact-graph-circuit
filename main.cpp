#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>
#include <map>
#include <iterator>
#include "Graphe.h"
#include "Valeur_Logique.h"
#include "PORTE_Base.h"

using namespace std;
/* partie 1
int main()
{
    int n;
    int et_output;
    PORTE_AND et;
    cout<<"Enter the numbre of inputs:"<<endl;
    cin>>n;
    et.Create_input(n);
    for (int i=0; i<n; i++){

        int tmp;
        cout<<"Enter each input for PORTE_ET:"<<endl;
        cin>>tmp;
        et.Set_input(tmp,i);
    }
    et_output=et.Calculate_output();
    cout<<"Output of PORTE_ET:"<<endl;
    cout<<et_output;

    return 0;
}
*/
int main() //partie 2,3
{
//-------------------lecture du fichier--------------------------
    GRAPHE Mygraphe; // definition de classe GRAPHE

    ifstream lire_fichier;
    lire_fichier.open("text.txt");
    int nlin=0,nmot=0;

    //stockage des lignes
    if (lire_fichier.is_open())
    {
        string var;
        cout<<"------------------------Le fichier txt:------------------------"<<endl;
        cout<<endl;
        while (getline(lire_fichier,var))
        {
            Mygraphe.ligne.push_back (var);
            cout<<Mygraphe.ligne[nlin]<<endl;
            nlin++;
        }
    }
    else
        cerr<<"Impossible d'ouvrir le fichier"<<endl;
    lire_fichier.close();

    //stokage des mots
    lire_fichier.open("text.txt");
    if (lire_fichier.is_open())
    {
        string var;
        char x;
        var.clear();
        x=lire_fichier.get();

        while (x!=EOF)
        {
            while (x!=' ' && x!=';' && x!='\n')
            {
                var=var+x;
                x=lire_fichier.get();
            }
            if (var.size()!=0)
            {
                Mygraphe.mot.push_back(var);
                nmot++;
            }
            var.clear();
            x=lire_fichier.get();
        }
        nmot=nmot-1; //Nombre des mots (-1 puisque nomt compte 1 plus dans la derniere operation)
    }
    else
        cerr<<"Impossible d'ouvrir le fichier"<<endl;
    lire_fichier.close();

    Mygraphe.Set_nlin_nmot(nlin,nmot);

 //------------------------------------------------------

 //------------------------------------------------------
    //calcul de nombre des entrees,nombre de sortie et nombre des portes
    int n_ent=0,n_sor=0,n_porte=0;

    for (int i=0;i<int(Mygraphe.ligne[0].length());i++)
        if (char(Mygraphe.ligne[0].at(i))=='E') n_ent++;


    for (int i=0;i<int(Mygraphe.ligne[1].length());i++)
        if (char(Mygraphe.ligne[1].at(i))=='S') n_sor++;

    n_porte=nlin-2;
    Mygraphe.Set_Info(n_ent,n_sor,n_porte);
    Mygraphe.Print_Info();
//--------------------------------------------------------------
//partie 4: lecture de fichier des valeurs logiques et declaration de classe Valeur Logique
    VALEUR MyValeur;
    int v_nlin=0;

    ifstream lire_valeur;
    lire_valeur.open("valeur_logique.txt");
    if (lire_valeur.is_open())
    {
        string var;
        int nom_vec;
        while (getline(lire_valeur,var))
        {
            MyValeur.Val_ligne.push_back(var);
           // cout<<MyValeur.Val_ligne[v_nlin]<<endl;
            char* ptr;
            const int len = var.length();
            ptr = new char[len+1];
            strcpy(ptr,var.c_str());

            char a[len-1];// un nombre de 4 chiffre au max
            for (int i=0;i<len;i++)
            {
                a[i]=*(ptr+i);
            }

            if(v_nlin==0)
            {
                nom_vec=int(a[0])-'0';
                for(int i=1;i<len;i++)
                {
                    nom_vec=nom_vec*10+int(a[i])-'0';
                }
                MyValeur.Set_NomVec(nom_vec);
            }
            else
            {
                char val;
                for(int i=0;i<len;i++)
                {
                    val=a[i];
                    MyValeur.Set_Val(val);
                }
                MyValeur.Set_NomVal(len);
            }
            v_nlin++;
        }
    }
    else
        cerr<<"Impossible d'ouvrir le fichier"<<endl;
    lire_valeur.close();

    MyValeur.Print_Info();

//-------------------Map Definition-------------------------
    map<string,int> MapDesMots;//definition de fonction map
    int N;
    N=n_ent+n_sor+n_porte;
    string nom[N];

    for (int i=0;i<n_ent;i++)
    {
        nom[i]='E';
        nom[i].push_back('0'+(i+1));
    }
    for(int i=0;i<n_porte;i++)
    {
        nom[i+n_ent]='P';
        nom[i+n_ent].push_back('0'+(i+1));
    }
    for(int i=0;i<n_sor;i++)
    {
        nom[i+n_ent+n_porte]='S';
        nom[i+n_ent+n_porte].push_back('0'+(i+1));
    }
    cout<<endl<<"-----------------Nom et Son indice equivalente:----------------"<<endl;
    for(int i=0;i<N;i++)
    {
        MapDesMots.insert(make_pair(nom[i],i));
        cout<<nom[i]<<":"<<i+1<<endl;
    }
    cout<<endl;
//-------------------------------------------------------
//definition de classe de base:
   // vector <PORTE_BASE> MyPortes;
    PORTE_BASE *P_MyPortes[n_porte];
    int it_portes=0; //itegrator de classe des portes creees

//remplir la matrice d'adjacence:
    Mygraphe.Init_Matrice();

    string *p;
    int nmot_d;
    int M_lin,M_col;
    string var;

    for (int i=0;i<nmot;i++)
    {
        var = Mygraphe.mot[i];
        if(var=="and"||var=="or"||var=="not"||var=="xor")
        {
            p = &Mygraphe.mot[i];
            nmot_d=i;
            break;
        }// cherche de premier mot pour les portes
    }
 //   P_MyPortes=&MyPortes[0];

    int ninput=0;//nombre input pour chaque porte
    int t=0; //pour chercher la fin de ligne

    do //de premier nom de porte jusqu'a dernier
    {
        string val;
        val=*p;

        int it; // indiquer le type de classe
        if(val=="and") it=0;
        else if (val=="or") it=1;
        else if (val=="not") it=2;
        else if (val=="xor") it=3;

        switch(it)
        {
            case 0:
            {
                P_MyPortes[it_portes]=new PORTE_AND;
                break;
            }
            case 1:
            {
                P_MyPortes[it_portes]=new PORTE_OR;
                break;
            }
            case 2:
            {
                P_MyPortes[it_portes]=new PORTE_NOT;
                break;
            }
            case 3:
            {
                P_MyPortes[it_portes]=new PORTE_XOR;
                break;
            }
            default:
                break;
        }
        p++;
        t++;
        M_col=MapDesMots.find(*p)->second;
        M_lin=M_col;
        val=*(++p);
        t++;

        if (val.substr(0,1)!="S")
        {
            MapDesMots.insert(make_pair(val,M_lin));
            p++;
            t++;
        }
        else
        {
            int M_cols;
            M_cols=MapDesMots.find(*p)->second;
            Mygraphe.Set_Matrice(M_lin,M_cols);
            p++;
            t++;
        }
        do
        {
            M_lin=MapDesMots.find(*p)->second;
            Mygraphe.Set_Matrice(M_lin,M_col);;
            t++;
            ninput++;
            if (t<=nmot-nmot_d) p++;
            else
                break;
        }while(*p!="and" && *p!="or" && *p!="not" && *p!="xor");

        P_MyPortes[it_portes]->Create_input(ninput); //mise a jour des variables de portes
        it_portes++;
        ninput=0;

    }while(t<=nmot-nmot_d);

//print matrix:
    Mygraphe.Print_Matrice();

//Mettre les valeurs logiques aux entrees de portes:
    Mygraphe.DFS_Methode(MyValeur,P_MyPortes);
/*
    string const nomFichier("E:\Study\UM-Robotique\TP C++\Sorties.txt");
    ofstream monFlux(nomFichier.c_str());

    if(monFlux)
    {
        monFlux <<"Les sorties"<<;

        for(int i(0); i < vout.size(); i++)
        {
            monFlux <<vout[i]<<endl;
        }
    }
    else
    {
        cout << "ERREUR: Impossible d'ouvrir le fichier." << endl;
    }
*/
    cout<<"Success! The End of Program!"<<endl;

    return 0;
}
