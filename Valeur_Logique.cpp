#include "Valeur_Logique.h"

using namespace std;

VALEUR::VALEUR() : Nombre_Val(0)
{

}

void VALEUR::Set_NomVec(int n)
{
    Nombre_Vec=n;
}

void VALEUR::Set_NomVal(int n)
{
    Nombre_Val=n;
}

void VALEUR::Set_Val(char c)
{
    Val.push_back(c);
}

void VALEUR::Print_Info()
{
     int N;
     N = Nombre_Val;
     string t[N];

     for(int i=0;i<N;i++)
     {
        t[i]='E';
        t[i].push_back('0'+i+1);
     }

     cout<<"---------------------Valeurs Logiques:---------------------"<<endl;
     cout<<"Nombres des vecteurs: "<<endl<<Nombre_Vec<<endl;
     cout<<"Valeurs Logiques: "<<endl;
     for(int i=0;i<N;i++)
     {
        cout<<t[i]<<" ";
     }
     cout<<endl;

     for(int i=0;i<Nombre_Vec;i++)
     {
        for(int j=0;j<Nombre_Val;j++)
        {
            cout<<Val[i+j]<<"  ";
        }
        cout<<endl;
     }
        cout<<endl;
}

VALEUR::~VALEUR()
{

}
