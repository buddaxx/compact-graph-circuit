#include "Porte_BASE.h"

using namespace std;

//DEFINITIONS DES METHODES PROTOTYPES DE LA PORTE DE BASE
//

//DEFINITIONS DES METHODES PROTOTYPES DE LA PORTE AND
PORTE_AND::PORTE_AND(): p_input(NULL), nombre_input(0), output(0)//Constructeur
{
    cout<<"Une porte AND vient d'etre creee"<<endl;
}

PORTE_AND::~PORTE_AND()
{
    delete p_input;
}

void PORTE_AND::Create_input(int n)
{
        p_input=new char[n];
        nombre_input=n;

        for(int i=0;i<n;i++) *(p_input+i)='0';
};

void PORTE_AND::print_info()
{
     cout<<"Porte AND"<<endl;
     cout<<"Les entrees de cette porte sont:"<<endl;
     for (int i=0;i<nombre_input;i++)
     {
         cout<<*(p_input+i)<<" ";
     }
     cout<<"Les sorties de cette porte sont:"<<output<<endl;
}

void PORTE_AND::Set_input(char val,int i)
{
    *(p_input+i)=val;
}

int PORTE_AND::Calculate_output()
{
        output=1;
        for (int i=0;i<nombre_input;i++)
        {
            if (*(p_input+i)=='0') output=0;
        }
        return output;
}

//DEFINITIONS DES METHODES PROTOTYPES DE LA PORTE OR
PORTE_OR::PORTE_OR():p_input(NULL),nombre_input(0),output(0)
{
        cout<<"Une porte OR vient d'etre cr\x82\x82e"<<endl;
}

void PORTE_OR::Create_input(int n)
{
        p_input=new char[n];
        nombre_input=n;
        for(int i=0;i<n;i++)  *(p_input+i)='0';
}

void PORTE_OR::print_info ()
{
        cout<<"Porte OR"<<endl;
        cout<<"Inputs of this porte are:"<<endl;
        for (int i=0;i<nombre_input;i++)
        {
            cout<<*(p_input+i)<<" ";
        }
        cout<<"Output of this porte is:"<<output<<endl;
}

void PORTE_OR::Set_input(char val,int i)
{
        *(p_input+i)=val;
}

int PORTE_OR::Calculate_output()
{
        output=0;
        for (int i=0;i<nombre_input;i++)
        {
            if (*(p_input+i)=='1')
                output=1;
        }
        return output;
}

PORTE_OR::~PORTE_OR()
{
    delete p_input;
}


//DEFINITIONS DES METHODES PROTOTYPES DE LA PORTE NOT
PORTE_NOT::PORTE_NOT(): p_input(NULL), output(0), nombre_input(1)
{
    cout<<"Une porte NOT vient d'etre cr\x82\x82e"<<endl;
}

void PORTE_NOT::Create_input(int n)
{
    p_input=new char[nombre_input];
    *p_input='0';
}

void PORTE_NOT::print_info ()
{
    cout<<"Porte NOT"<<endl;
    cout<<"L'entree de cette porte est:"<<endl;
    for (int i=0;i<nombre_input;i++)
    {
        cout<<*(p_input+i)<<" ";
    }
    cout<<"La sortie de cette porte est:"<<output<<endl;
}

void PORTE_NOT::Set_input (char val,int i)
{
    *(p_input+i)=val;
}

int PORTE_NOT::Calculate_output()
{
    if(*p_input=='1') output=0;
    if(*p_input=='0') output=1;
    return output;
}

PORTE_NOT::~PORTE_NOT()
{
    delete p_input;
}

//DEFINITIONS DES METHODES PROTOTYPES DE LA PORTE XOR
PORTE_XOR::PORTE_XOR() : p_input(NULL), nombre_input(2), output(0)
{
        cout<<"Une porte XOR vient d'etre creee"<<endl;
}

void PORTE_XOR::Create_input(int n)
{
    p_input=new char[nombre_input];
    *p_input='0';
    *(p_input+1)='0';
}

void PORTE_XOR::print_info()
{
    cout<<"Porte XOR"<<endl;
    cout<<"Inputs of this porte are:"<<endl;
    for (int i=0;i<nombre_input;i++)
    {
        cout<<*(p_input+i)<<" ";
    }
    cout<<"Output of this porte is:"<<output<<endl;
}

void PORTE_XOR::Set_input(char val,int i)
{
        *(p_input+i)=val;
}

int PORTE_XOR::Calculate_output()
{
	    int a,b;
	    a=int(*p_input-48);
	    b=int(*(p_input+1)-48);
        output=a^b;
        return output;
}

PORTE_XOR::~PORTE_XOR()
{
    delete p_input;
}
