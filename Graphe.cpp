#include "Graphe.h"
#include <fstream>

GRAPHE::GRAPHE(): n_entree(0), n_sortie(0), n_porte(0)//Initialisation par defaut
{

}

void GRAPHE::Set_Info(int n_ent, int n_sor, int n_por)
{
    n_entree = n_ent;
    n_sortie = n_sor;
    n_porte = n_por;
}

void GRAPHE::Set_nlin_nmot(int nl, int nm)
{
    nlin = nl;
    nmot = nm;
}

void GRAPHE::Set_Matrice(int mlin, int mcol)
{
    M[mlin][mcol] = 1;
}

void GRAPHE::Init_Matrice()
{
            N=n_entree+n_sortie+n_porte;

            for (int j=0;j<N;j++) // initialisation de matrice
            {
                vector<int> temp;
                for (int i=0;i<N;i++)
                {
                    temp.push_back(0);
                }
                M.push_back(temp);
            }
            cout<<"Initialisation de matrice avec success"<<endl;
}

void GRAPHE::Print_Info()
{
        if (n_entree==0||n_sortie==0) cerr<<"Le fichier des donnees n'est pas correcte"<<endl;

        cout<<endl<<"------------------------Information:--------------------------"<<endl;
        cout<<endl<<"Nombre des entrees: "<<n_entree<<endl;
        cout<<"Nombre des sorties: "<<n_sortie<<endl;
        cout<<"Nombre des portes: "<<n_porte<<endl;
}

void GRAPHE::Print_Matrice()
{
        cout<<endl<<"--------------------Matrice d'adjacence:----------------------"<<endl<<endl;

        for(int i=0;i<N;++i)
        {
            for(int j=0;j<N; ++j)
            {
                cout<<M[i][j]<<" ";
            }
            cout<<endl;
        }
        cout<<endl;
}

void GRAPHE::DFS_Methode(VALEUR &MyValeur, PORTE_BASE *P_MyPortes[])
{
        int it_vector=0;

        //string const nomFichier(".//circuit_graphe_compact//Sorties.txt");//Chemin relatif de sauvegarde de la sortie
        ofstream monFlux("Sorties.txt");

        //string Sorties[MyValeur.Nombre_Vec-1];
        char Ent[n_entree];
        char Sort[n_sortie*MyValeur.Nombre_Vec];
        char Port[n_porte];

        do
        {
            int n_input=0;

            for(int i=0;i<n_entree;i++)
            {
                Ent[i]=MyValeur.Val[i+n_entree*it_vector];
            }
            int mlin,mcol;
            for(int i=0;i<n_porte;i++)
            {
                mcol=i+n_entree;
                for(int j=0;j<N;j++)
            {
                mlin=j;
                if(M[mlin][mcol]==1)
                {
                    if(j<n_entree)
                    {
                        //  if (n_input>=(P_MyPortes[i]->nombre_input))
                        //  cerr<<"Error : No matching inputs numbers"<<endl;
                        //  P_MyPortes[i]->print_info();
                        P_MyPortes[i]->Set_input(Ent[j],n_input);
                        //    P_MyPortes[i]->print_info();
                        n_input++;
                    }
                    if(j>=n_entree&&j<n_entree+n_porte)
                    {
                        //  if (n_input>=(*P_MyPortes[i]->nombre_input))
                        //  cerr<<"Error : No matching inputs numbers"<<endl;
                        int t=j-n_entree;
                        P_MyPortes[i]->Set_input(Port[t],n_input);
                        n_input++;
                    }
                }

            }
                    n_input=0;
                    Port[i]=char(P_MyPortes[i]->Calculate_output()+48);
                   // P_MyPortes[i]->print_info();
            }
                for(int i=0;i<n_sortie;i++)
                {
                    int mcols=i+n_entree+n_porte;
                    for(int j=0;j<N;j++)
                    {
                        int mlins=j;
                        if(M[mlins][mcols]==1)
                        {
                            Sort[(i+it_vector*n_sortie)]=Port[mlins-n_entree];
                           // cout<<endl<<"Sortie courante:"<<Sort[i+it_vector*n_sortie]<<endl;
                        }
                    }
                }
                it_vector++;
            }while(it_vector<MyValeur.Nombre_Vec);

            cout<<"---------------------Les Sorties de ce Graphe:---------------------"<<endl;
            cout<<endl;
            for(int i=0;i<MyValeur.Nombre_Vec;i++)
            {
                cout<<"Les sorties pour "<<i+1<<" vecteur d'entrees: ";
                for(int j=0;j<n_sortie;j++)
                {
                    cout<<"S"<<j+1<<":"<<Sort[j+i*n_sortie]<<" ";
                    if(monFlux) monFlux << Sort[j+i*n_sortie];
                    else
                    {
                        cout << "ERREUR: Impossible d'ouvrir le fichier." << endl;
                    }
                }
                monFlux<<endl;
                cout<<endl;
            }
cout<<endl;
}

GRAPHE::~GRAPHE()
{

}

