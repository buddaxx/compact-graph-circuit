#ifndef VALEUR_LOGIQUE_H_INCLUDED
#define VALEUR_LOGIQUE_H_INCLUDED

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

class VALEUR{

    private:
        int Nombre_Vec;
        int Nombre_Val;
        vector <char> Val;

    public:

        vector <string> Val_ligne;

        VALEUR();
        ~VALEUR();

        friend class GRAPHE;

        void Set_NomVec(int n);
        void Set_NomVal(int n);
        void Set_Val(char c);
        void Print_Info();
};

#endif // VALEUR_LOGIQUE_H_INCLUDED
